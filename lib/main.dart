import 'package:flutter/material.dart';
import 'package:get/get.dart';


import 'package:tape_out/screens/home/home.dart';
import 'package:tape_out/screens/loved/loved_page.dart';
import 'screens/login/Sign_in_screen.dart';

void main() {
  runApp(GetMaterialApp(
    initialRoute: '/',
    namedRoutes: {
      '/': GetRoute(page: SignInPage()),
      '/home': GetRoute(page: Home()),
      '/loved': GetRoute(page: LovedSongs()),
    },
  ));
}
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Center(),
    );
  }
}