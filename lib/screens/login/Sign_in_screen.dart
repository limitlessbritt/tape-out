import 'dart:html';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:tape_out/api/api_call.dart';
import 'package:tape_out/api/auth.dart';
import 'package:tape_out/api/recently_added_albums_call.dart';
import 'dart:convert';

import 'package:get/get.dart';
import 'package:tape_out/screens/home/home.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}
  TextEditingController passwordController;
  TextEditingController usernameController;
  TextEditingController serveraddressController;
  String password;
  String username;
  String serveraddress;
  bool _obsecure = false;


class _SignInPageState extends State<SignInPage> {
  @override
  void initState() {
    passwordController = TextEditingController();
    usernameController = TextEditingController();
    serveraddressController = TextEditingController();
    passwordController.addListener(() {
      setState(() {});
    });
    usernameController.addListener(() {
      setState(() {});
    });
    serveraddressController.addListener(() {
      setState(() {});
    });
    super.initState();
  }
    @override
  void dispose() {
    passwordController.dispose();
    usernameController.dispose();
    serveraddressController.dispose();
    super.dispose();
  }
void loginUser() {
serveraddress = serveraddressController.text;
password = passwordController.text;
username = usernameController.text;
print(username);
print(password);
print(serveraddress);
}

void loginclear(){
serveraddressController.clear();
passwordController.clear();
usernameController.clear();
}

void userClear(){
serveraddress = null;
password = null;
username = null;
}

  @override
  Widget build(BuildContext context) {
      return Scaffold(
      backgroundColor: Colors.black,
      body: ListView(
        children: <Widget>[
              Column(
              children: <Widget>[
              Container(
              margin: EdgeInsets.only(top: 50.00),
              width: double.infinity,
              height: 80.00,
              child: Icon(Icons.music_note, size: 100.00, color: Colors.white,),
            ),
            Container(
              /* color: Colors.white, */
              height: 60.00,
              width: double.infinity,
              margin: EdgeInsets.only(top: 30.00),
              child: Text("Welcome to Tape-Out", style: TextStyle(fontSize: 30.00, color: Colors.white),textAlign: TextAlign.center,),
            ),
            Container(
              /* color: Colors.white, */
              height: 50.00,
              width: double.infinity,
              margin: EdgeInsets.only(top: 1.00),
              child: Text("Please connet to your music server.", style: TextStyle(fontSize: 20.00, color: Colors.white),textAlign: TextAlign.center,),
            ),
            Container(
              height: 30.00,
              width: double.infinity,
              margin: EdgeInsets.only(top: 5.00),
              child: Text("Server Address:", style: TextStyle(fontSize: 15.00, color: Colors.white),textAlign: TextAlign.center,),
            ),
              Container(
              height: 50.00,
              child: input(Icon(Icons.dns), "http://", serveraddressController, false),
            ),
            Container(
              height: 30.00,
              width: double.infinity,
              margin: EdgeInsets.only(top: 10.00),
              child: Text("Username:", style: TextStyle(fontSize: 15.00, color: Colors.white),textAlign: TextAlign.center,),
            ),
              Container(
              height: 50.00,
              margin: EdgeInsets.only(top: 10.00),
              child: input(Icon(Icons.perm_identity), "", usernameController, false),
            ),
              Container(
              height: 30.00,
              width: double.infinity,
              margin: EdgeInsets.only(top: 20.00),
              child: Text("Password:", style: TextStyle(fontSize: 15.00, color: Colors.white),textAlign: TextAlign.center,),
            ),
              Container(
              height: 50.00,
              margin: EdgeInsets.only(top: 1.00),
              child: input(Icon(Icons.vpn_key), "", passwordController, true),
            ),
            Container(
              height: 50.00,
              margin: EdgeInsets.only(top: 10.00),
              child: RaisedButton(
              color: Colors.black,
              child: Text(
              "Connect",
              style: TextStyle(
              fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
              loginUser();
              fetchResponse().then((response){
                if(response.status =="ok"){
                  return Get.toNamed('/home');
                }else{
                  return showAlertDialog(context);
                }
              });
        },
      )
            ),
                ],
              )
        ],
      )
    );
  }
  }

Widget input(Icon icon, String hint, TextEditingController controller, bool obsecure) {
      return Container(
        padding: EdgeInsets.only(left: 20, right: 20),
        child: TextField(
          controller: controller,
          obscureText: obsecure,
          style: TextStyle(fontSize: 20, color: Colors.white),
          decoration: InputDecoration(
              hintStyle: TextStyle(fontSize: 20, color: Colors.grey[850]),
              hintText: hint,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Colors.grey[850],
                  width: 2,
                ),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide(
                  color: Colors.grey[850],
                  width: 3,
                ),
              ),
              prefixIcon: Padding(
                child: IconTheme(
                  data: IconThemeData(color: Colors.grey[850]),
                  child: icon,
                ),
                padding: EdgeInsets.only(left: 30, right: 10),
              )),
        ),
      );
    }




showAlertDialog(BuildContext context) {

  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
    Navigator.of(context).pop();
     },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Error"),
    content: Text("Wrong credentials. Please try again."),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

