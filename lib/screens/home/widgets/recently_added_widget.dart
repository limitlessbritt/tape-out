import 'package:flutter/material.dart';
import 'package:tape_out/api/api_call.dart';
import 'package:tape_out/api/recently_added_albums_call.dart';


class RecentlyAddedWidget extends StatefulWidget{
  @override
  _RecentlyAddedWidgetState createState() => _RecentlyAddedWidgetState();
}
class _RecentlyAddedWidgetState extends State<RecentlyAddedWidget> {
  final Future<List<Album>> albumsresponse = fetchRecentlyAddedAlbums();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: FutureBuilder(
      future: fetchRecentlyAddedAlbums(),
      builder: (context, AsyncSnapshot<List<Album>> data) {
        switch (data.connectionState) {
          case ConnectionState.none:
            return Text('none');
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.active:
            return Text('');
          case ConnectionState.done:
            if (data.hasData) {
              List<Album> albumsresponse = data.data;
              return ListView.builder(
                itemCount: albumsresponse.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(
                      albumsresponse[index].title,
                      style: TextStyle(fontSize: 25.0),
                    ),
                  );
                },
              );
            }
        }
      },
    )));
  }
}
