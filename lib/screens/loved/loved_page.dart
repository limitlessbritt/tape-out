import 'package:flutter/material.dart';
import 'package:tape_out/api/api_call.dart';
import 'package:tape_out/api/loved_call.dart';

class LovedSongs extends StatefulWidget{
  @override
  _LovedSongsState createState() => _LovedSongsState();
}
class _LovedSongsState extends State<LovedSongs>{
  final Future<List<Song>> songs = fetchStarred();
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        backgroundColor: Colors.black,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("Favorites"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            color: Colors.white,
            onPressed: (){
              print("setting");
            }
          ),
        ],
      ),
      body: Container(
        child: FutureBuilder(
          future: fetchStarred(),
          builder: (context, AsyncSnapshot<List<Song>> data){
            switch (data.connectionState){
              case ConnectionState.none:
                return Text("none", style: TextStyle(color: Colors.white),);
              case ConnectionState.waiting:
                return Center(child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.white),));
              case ConnectionState.active:
                return Text('');
              case ConnectionState.done:
              if (data.hasData){
                List<Song> songs = data.data;
                return ListView.builder(
                  itemCount: songs.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(songs[index].title, style: TextStyle(fontSize: 25.0, color: Colors.white),),
                      subtitle: Text(songs[index].artist, style: TextStyle(fontSize: 15.0, color: Colors.white),),
                    );
                  },
                );
              }
            }
          },
        ),
      )

    );
  }
}

