import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:math';
import 'package:crypto/crypto.dart';
import 'package:tape_out/api/recently_added_albums_call.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

import 'package:tape_out/screens/login/Sign_in_screen.dart';
import 'package:tape_out/model/user.dart';

import 'api_call.dart';

String rest = "/rest/";
String tapeOutVerison = "1.13.0";
String client = "TapeOut";
String salt = randomToken(6);
String token = makeToken(password, salt);
String format = "&f=json";
String ping = "ping";

final _random = Random();

// Salt
String randomToken(int length) => String.fromCharCodes(
      List.generate(length, (_) {
        var ch = _random.nextInt(52);
        if (ch > 25) {
          ch += 6;
        }
        return ch + 0x41;
      }),
    );

String newSalt() => randomToken(6);

// Token
String makeToken(String password, String salt) =>
    md5.convert(utf8.encode(password + salt)).toString().toLowerCase();


String baseURL =  serveraddress + rest + ping + "?" + "u=" + username + "&t=" + token + "&s=" + salt + "&v=" + tapeOutVerison + "&c=" + client + format;

String authURL = serveraddress+ rest + ping + "?" + "u=" + username + "&t=" + token + "&s=" + salt + "&v=" + tapeOutVerison + "&c=" + client + format;

String userCall = serveraddress + rest + "getUser/" + "?" + "u=" + username + "&t=" + token + "&s=" + salt + "&v=" + tapeOutVerison + "&c=" + client +  "&username=" + username + format;


Future <SubsonicResponse>fetchResponse() async{
  try{
var authresponse = await http.get(authURL);
if (authresponse.statusCode == 200){
var jsondata = jsonDecode(authresponse.body);
var data = apicallFromJson(jsondata);
var response = data.subsonicResponse;
return response;
} else{
}
}
 catch (e){
    print(e);
    }
}


Future <User>fetchUser() async{
var authresponse = await http.get(userCall);
if (authresponse.statusCode == 200){
var jsondata = jsonDecode(authresponse.body);
final data = apicallFromJson(jsondata);
var  user = data.subsonicResponse.user;
return user;
}else{
throw Exception("Unable to connect to server, try again");}
}





