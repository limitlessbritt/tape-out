import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:tape_out/api/api_call.dart';


Future <List<Song>>fetchStarred() async{
final authresponse = await http.get(lovedsongsURL);
if (authresponse.statusCode == 200){
final jsondata = jsonDecode(authresponse.body);
var songsdata = apicallFromJson(jsondata);
var starred2 = songsdata.subsonicResponse.starred2.song;
return starred2;
}else
throw Exception("Unable to connect to server, try again");
}