import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'api_call.dart';
import 'package:tape_out/screens/login/Sign_in_screen.dart';

import 'auth.dart';

String recentAlbumsURL = serveraddress + rest + "/getAlbumList/" + "?" + "u=" + username + "&t=" + token + "&s=" + salt + "&v=" + tapeOutVerison + "&c=" + client +  "&type=newest";


Future<List<Album>>fetchRecentlyAddedAlbums() async{
  try{
    var response = await http.get(recentAlbumsURL);
    if (response.statusCode == 200){
      var jsondata = jsonDecode(response.body);
      var data = apicallFromJson(jsondata);
      var recentAResponse = data.subsonicResponse.albumList.album;
      return recentAResponse;
    }else{
      print("error");
    }

  }
  catch (e){
    print(e);
  }
}

String coverArtID = "";

String recentAlbumsArtURL = serveraddress + rest + "/getCoverArt/?" + "u=" + username + "&t=" + token + "&s=" + salt + "&v=" + tapeOutVerison + "&c=" + client + "&id=" + coverArtID;

Future<dynamic>fetchCoverArt(){
fetchRecentlyAddedAlbums().then((recentAResponse){
});
}