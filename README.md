# Tape-Out
![Flutter CI](https://github.com/limitlessbritt/Tape-Out/workflows/Flutter%20CI/badge.svg)
[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
[![GitHub release](https://img.shields.io/github/release/Naereen/StrapDown.js.svg)](https://github.com/limitlessbritt/Tape-Out/releases)
[![GitHub issues](https://img.shields.io/github/issues/Naereen/StrapDown.js.svg)](https://github.com/limitlessbritt/Tape-Out/issues/)
[![Open Source Love svg1](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![CodeFactor](https://www.codefactor.io/repository/github/limitlessbritt/tape-out/badge?s=d373336ea7057175607633dc01188deb3d37b9bf)](https://www.codefactor.io/repository/github/limitlessbritt/tape-out)

This is a moblie client app for Subsonic API compatible servers.
Written in Flutter
Tested with Navidrome & Airsonic

## Why?
I wanted something modern looking that also combines features from multiple apps to make the app in one app. 
## Features
* Album View

## Installation
Download the app via the PlayStore, f-droid or in the releases page.

## To Do

## Contributions

## Acknowledgements

## License

